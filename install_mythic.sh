#!/bin/bash
# -----------------------------------------------------------------------------

MYTHIC_DIR="/opt/dev/Mythic"
MYTHIC_URL="https://github.com/its-a-feature/Mythic"
MYTHIC_PROFILE="https://github.com/MythicC2Profiles/http"

# -----------------------------------------------------------------------------

if [ "$EUID" -ne 0 ]; then
  echo "Please sudo/run as root (sudo bash $0)"
  exit 1
fi

# -----------------------------------------------------------------------------

if [ -f "$MYTHIC_DIR/mythic-cli" ]; then
  $MYTHIC_DIR/mythic-cli stop
  rm -rf "$MYTHIC_DIR"
fi

# -----------------------------------------------------------------------------

apt-get update -q -y

# -----------------------------------------------------------------------------

mkdir -p "$MYTHIC_DIR" 
git clone "$MYTHIC_URL" "$MYTHIC_DIR" 
cd "$MYTHIC_DIR" 
./install_docker_debian.sh 
make 

# ---------------------------------------------------------------------------

./mythic-cli install github $MYTHIC_PROFILE -f

for agentFolder in "$CI_PROJECT_DIR"/agents/*; do
  if [ -d "$agentFolder" ]; then
    ./mythic-cli install folder "$agentFolder" -f
  fi
done

# -----------------------------------------------------------------------------

./mythic-cli start
grep -E 'MYTHIC_ADMIN_(USER|PASSWORD)' .env
