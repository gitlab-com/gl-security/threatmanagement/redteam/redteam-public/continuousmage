# ContinuousMage

This project enables continuous testing of Mythic-compatible C2 agents. Its initial design details and implementation instructions, especially around GitLab CI/CD, can be found on the [GitLab blog.](https://about.gitlab.com/blog/2023/11/28/how-gitlabs-red-team-automates-c2-testing/) Although the implementation has changed since that blog post, the intent behind the project remains the same. The post also has some useful information on setting up project prerequisites.

We highly suggest copying this repository and creating your own private repo in GitLab.com or your own GitLab instance if you intend on leveraging the project's CI/CD functionality. Because this project utilizes a [shell executor](https://docs.gitlab.com/runner/security/index.html#usage-of-shell-executor) for its GitLab runner, you will want to run this project in a trusted and controlled environment.

We also like to thank Cody Thomas ([@its_a_feature_](https://twitter.com/its_a_feature_)), whose contributions have substantially improved the project.

## Get the Code

Clone this repository using `git clone --recurse-submodules` to make sure you get the agent code.
If you have already cloned the repo and didn't get the submodules, you'll need to run a couple extra commands.

```bash
git submodule init
git submodule update
```

Check and modify the YAML file to point to the correct Mythic install directory and run the gitlab runner as a user with passwordless sudo.
Then, make sure that your machine has the python requirements that are needed.

```bash
python3 -m pip install -r mythic-test/requirements.txt
```

## Add Your Own Agent

To add your own agent to your copy of this repo, you first need to add it as a submodule.

```bash
cd agents
git submodule add $AGENT_REPO_URL
```

## Updating the Existing Agents

To update an existing agent in the repository to a new version, you'll leverage git's submodule command.

```bash
git submodule update --init agents/poseidon
git submodule update --remote agents/poseidon
git add agents/poseidon
git commit -m "Update poseidon to vX.Y.Z"
```

## Run Tests Without CI/CD

There are a few prerequisites required

- python requirements installed
- Mythic installed
- Your copy of the repo installed on the Mythic server

PyTest takes a few command line arguments to configure aspects of the testing (Note: environment and .env settings override commandline settings):

- `--username` if you want to auth as somebody other than `mythic_admin`
  - `USERNAME` in env or .env
- `--password` if the password for user is something other than `mythic_password`
  - `MYTHIC_ADMIN_PASSWORD` in env or .env
- `--server-ip` if the ip address of Mythic is something other than `127.0.0.1`
  - `SERVER_IP` in env or .env
- `--server-port` if the port for Mythic is something other than `7443`
  - `SERVER_PORT` in env or .env
- `--apitoken` if you want to auth with apitoken instead of username/password combination
  - `APITOKEN` in env or .env
- `--timeout` to specify a timeout for actions to occur
  - `TIMEOUT` in env or .env
- `--payload-types` to specify your payload types
  - `PAYLOAD_TYPES` in env or .env
- `--operating-systems` to specify your operating systems
  - `OPERATING_SYSTEMS` in env or .env
- `--architectures` to specify your architectures
  - `ARCHITECTURES` in env or .env
- `--runbuild` to specify 'allowing' the tests to build a new payload. If this isn't specified than anything that requires building a payload will be skipped
  - `RUNBUILD` in env or .env
- `--runslow` to specify 'allowing' the tests marked as slow to execute
  - `RUNSLOW` in env or .env
- `--payload-uuid` to specify an already created payload UUID to use instead of building a new one
  - `PAYLOAD_UUID` in env or .env
- `--callback-display-id` to specify an already existing callback to use instead of generating a new one based on the payload uuid
  - `CALLBACK_DISPLAY_ID` in env or .env

If a payload needs to be created and a `--payload-uuid` isn't specified, then the tests will look in `supporting-files` for `{payload type}_build.json` for the configuration for building your payload.

All tests should be matched up to the associated payload type and in their corresponding module. Make sure if you're creating tests for a new agent that you make it a new folder with a `__init__.py` file so it's a proper module.

Our pytest suite expects to be run from the `mythic-test/` directory. As an example, to run the tests specific to Poseidon on x64 Linux, you'll run the following:

```bash
cd mythic-test
pytest ./ --payload-types poseidon --architectures amd64 --operating-systems Linux
```

## Run Tests with CI/CD

**NOTE:** The CI/CD code in this repository is example code. In order to run your own instance of this test suite with CI/CD, you will need to make your own copy of the project and configure it to use your infrastructure.

The CI/CD pipeline example in this repository now handles everything from Mythic install through running the actual pytests. It relies on you already having a working Linux GitLab runner which is configured to pick up jobs from your copy of this project. This runner is where your Linux agent tests will run. In a future version, we will automate running tests across multiple GitLab runners, so you can run all of the tests from CI/CD. See the original blog post linked at the top of the README for instructions on setting up these prerequisites.

Installing Mythic automatically requires us to try to uninstall Mythic if it's already running. In order to give the user as much control as possible over their environment, we have changed the pipeline to run manually, versus running on MRs and tagged releases. This allows users to make MRs and improve their tests without forcing full Mythic reinstalls or upgrades. Both the `build` stage (builds Mythic) and `install` stage (installs your agent into Mythic) must be triggered manually. The `test` stage will run automatically after a successful `install`. In this way, you can run `install` and `test` without running `build`.

## Develop New Tests

Running the full pytest suite can take a few minutes, which slows down the process of writing new tests.
To just test a single function run `pytest -k [function name]` and to run all tests in just one file use `pytest -k filename.py`.
You can also use your own marker to decorate your function and execute only those ( `pytest -m [custom marker]`); this is helpful if you have multiple you want to run.
More info can be found [here](https://stackoverflow.com/questions/36456920/specify-which-pytest-tests-to-run-from-a-file).

### Required New Test Format

To start testing a new payload type, you need to create a module under the `mythic-test` folder with the payload type name (i.e. make a folder with the name of your payload type and include an empty `__init__.py` file in there).
Within that folder, you need to create another folder called `supporting-files` which is where you have all of your additional pieces of data needed in your tests. In here, you should have at least one instance of `{payload_name}_build.json` which can be used to automatically build an instance of your payload.
You can specify in your modules (i.e. any file within the folder named after your payload type) for a different build file to be used by specifying a `build_file` variable at the top that points to the `supporting-files/filename` to use instead.

#### Create a new payload test from template

After cloning the repository, copy the template directory into the [mythic-test](mythic-test) directory:

```sh
cp -R \
  template/example \
  mythic-test/example
```

### Debugging

Have an issue with a test you're writing? Add in `pytest.set_trace()` where you want to stop and run `pytest [your normal flags] --pdb`. This will drop you into a python debugger where you can examine variables and see what's going wrong.

## Contributing

At GitLab, we always seek feedback on our work. If you have any questions or comments, please open an issue on this project. We always welcome improvements via a merge request. Please do your work in a feature branch, and then open a MR for your changes. We believe that everyone should be able to contribute, so we welcome any contributions, big or small.
