from fabric import Connection, Result
import uuid
import asyncio
import pytest


async def remote_execution_function(payload_path: str,
                                    host: str,
                                    password: str,
                                    user: str) -> (Connection, Result, str):
    if host is None or host == "":
        pytest.fail("need to run test against remote host but none specified")
    if user is None or user == "":
        pytest.fail("need to run test against remote host but no user specified")
    conn = Connection(host, connect_kwargs={
        "password": password,
    }, user=user)
    remote_path = f"/tmp/{uuid.uuid4()}"
    conn.sftp().put(payload_path, remote_path, confirm=True)
    conn.run(f"chmod +x {remote_path}")
    conn.run(f"{remote_path} &>/dev/null &")
    await asyncio.sleep(2)  # wait a few seconds for the agent to start up and connect
    running_result = conn.run(f"ps -ef | grep -i {remote_path} | grep -v grep")
    running_pieces = [x for x in running_result.stdout.split("\n") if x != ""]
    # example macos output: bottom two will likely always be there, but first one is the actual agent running
    # 502 84444     1   0 12:40PM ??         0:00.44 /tmp/0758b328-7908-4ca0-9406-d20fbcdab3d8
    # 502 84447 84440   0 12:40PM ??         0:00.01 zsh -c ps -ef | grep -i /tmp/0758b328-7908-4ca0-9406-d20fbcdab3d8
    # 502 84449 84447   0 12:40PM ??         0:00.00 grep -i /tmp/0758b328-7908-4ca0-9406-d20fbcdab3d8
    # example linux output:
    # its-a-f+  59984      1 64 10:39 ?        00:00:01 /tmp/b12f1416-8c49-4217-b1bf-b70f20784615
    # its-a-f+  60012  59945  0 10:39 ?        00:00:00 bash -c ps -ef | grep -i /tmp/b12f1416-8c49-4217-b1bf-b70f20784615
    # its-a-f+  60014  60012  0 10:39 ?        00:00:00 grep -i /tmp/b12f1416-8c49-4217-b1bf-b70f20784615
    if len(running_pieces) != 1:
        pytest.fail("The agent isn't running on the remote host")
    return conn, running_result, remote_path


async def remote_cleanup_function(remote_result: (Connection, Result, str)) -> None:
    remote_result[0].run(f"rm {remote_result[2]}")
    running_pieces = remote_result[1].stdout.split("\n")[0].split(" ")
    running_pieces = [x for x in running_pieces if x != ""]
    remote_result[0].run(f"kill -9 {running_pieces[1]}")
    remote_result[0].close()
