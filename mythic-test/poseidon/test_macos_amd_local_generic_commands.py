from mythic import mythic
import pytest
import os
import json


@pytest.fixture
async def custom_random_path(request, random_file_path):
    if request.param is not None:
        return request.param
    return random_file_path


@pytest.mark.os("macOS")
@pytest.mark.parametrize(("custom_random_path", "contents"), [
    pytest.param("/path/doesn't/exist", b"this is a test", marks=pytest.mark.xfail),
    pytest.param(None, b"this is a test"),
], indirect=["custom_random_path"], ids=["bad_path", "valid_path"])
async def test_upload(mythic_instance, task_timeout, callback_display_id, custom_random_path, contents):
    resp = await mythic.register_file(
        mythic=mythic_instance, filename="poseidon/test_generic_commands::test_upload", contents=contents
    )
    status = await mythic.issue_task_and_waitfor_task_output(
        mythic=mythic_instance,
        command_name="upload",
        parameters={"remote_path": custom_random_path, "file_id": resp, "overwrite": True},
        callback_display_id=callback_display_id,
        timeout=task_timeout
    )
    assert os.path.getsize(custom_random_path) == len(contents)


@pytest.mark.os("macOS")
async def test_ls(mythic_instance, task_timeout, callback_display_id):
    # issue the task and wait for it to finish
    task = await mythic.issue_task(
        mythic=mythic_instance,
        command_name="ls",
        parameters="",
        callback_display_id=callback_display_id,
        token_id=None,
        wait_for_complete=True,
        custom_return_attributes="""
            id
            status
            completed
            display_id
            callback {
                id
            }
        """,
        timeout=task_timeout
    )
    # gather all the task's output
    output = await mythic.waitfor_for_task_output(
        mythic=mythic_instance,
        task_display_id=task["display_id"],
        timeout=task_timeout,
    )
    # assert that the output is as expected
    output = json.loads(output)
    assert "name" in output
    assert output["success"]
    # now make sure the data made it into the file browser
    tree_output = await mythic.execute_custom_query(mythic=mythic_instance,
                                                    query="""
       query getFileBrowserData($callback_id: Int!, $task_id: Int!){
        mythictree(where: {callback_id: {_eq: $callback_id}, tree_type: {_eq: "file"}, task_id: {_eq: $task_id}}){
            name_text
            task_id
        }
       }
                                               """,
                                                    variables={"callback_id": task["callback"]["id"],
                                                               "task_id": task["id"]})
    assert len(tree_output["mythictree"]) >= len(output["files"]) + 1


@pytest.mark.os("macOS")
async def test_list_entitlements(mythic_instance, task_timeout, callback_display_id):
    output = await mythic.issue_task_and_waitfor_task_output(
        mythic=mythic_instance,
        command_name="list_entitlements",
        parameters=json.dumps({"pid": 1}),
        callback_display_id=callback_display_id,
        timeout=task_timeout,
    )
    output = json.loads(output)
    assert len(output) > 0
