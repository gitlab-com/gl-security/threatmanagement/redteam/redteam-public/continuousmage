import os
import pytest
from mythic import mythic
from . import helper_functions
import json
from uuid import uuid4
import requests



async def wrapped_remote_execution_function(payload_path: str):
    return await helper_functions.remote_execution_function(payload_path=payload_path,
                                                            host=os.getenv("LINUX_X64_REMOTE_HOST"),
                                                            password=os.getenv("LINUX_X64_REMOTE_PASSWORD"),
                                                            user=os.getenv("LINUX_X64_REMOTE_USER"))
    

@pytest.mark.os("Linux")
@pytest.mark.arch("amd64")
async def test_ls(mythic_instance, task_timeout, make_callback_display_id):
    async for callback_id in make_callback_display_id(
            use_payload_build_file="poseidon_linux_x64_build.json",
            # use_payload_uuid="4faadb9c-4a3c-4c6f-b4f8-343965335a29",
            remote_execution_function=None,#wrapped_remote_execution_function,
            remote_cleanup_function=None#helper_functions.remote_cleanup_function
    ):
        # issue the task and wait for it to finish
        task = await mythic.issue_task(
            mythic=mythic_instance,
            command_name="ls",
            parameters="",
            callback_display_id=callback_id,
            token_id=None,
            wait_for_complete=True,
            custom_return_attributes="""
                id
                status
                completed
                display_id
                callback {
                    id
                }
            """,
            timeout=task_timeout
        )
        # gather all the task's output
        output = await mythic.waitfor_for_task_output(
            mythic=mythic_instance,
            task_display_id=task["display_id"],
            timeout=task_timeout,
        )
        # assert that the output is as expected
        output = json.loads(output)
        assert "name" in output
        assert output["success"]
        # now make sure the data made it into the file browser
        tree_output = await mythic.execute_custom_query(mythic=mythic_instance,
                                                        query="""
           query getFileBrowserData($callback_id: Int!, $task_id: Int!){
            mythictree(where: {callback_id: {_eq: $callback_id}, tree_type: {_eq: "file"}, task_id: {_eq: $task_id}}){
                name_text
                task_id
            }
           }
                                                   """,
                                                        variables={"callback_id": task["callback"]["id"],
                                                                   "task_id": task["id"]})
        assert len(tree_output["mythictree"]) >= len(output["files"]) + 1


use_payload_build_file = "poseidon_linux_x64_build.json"
remote_execution_function = None #wrapped_remote_execution_function
remote_cleanup_function = None #helper_functions.remote_cleanup_function


# use a module-wide instance of a callback, callback_display_id, based on the above module parameters
@pytest.mark.os("Linux")
@pytest.mark.arch("amd64")
async def test_pwd(mythic_instance, task_timeout, callback_display_id):
    # issue the task and wait for it to finish
    task = await mythic.issue_task(
        mythic=mythic_instance,
        command_name="pwd",
        parameters="",
        callback_display_id=callback_display_id,
        token_id=None,
        wait_for_complete=True,
        custom_return_attributes="""
            id
            status
            completed
            display_id
            callback {
                id
            }
        """,
        timeout=task_timeout
    )
    # gather all the task's output
    output = await mythic.waitfor_for_task_output(
        mythic=mythic_instance,
        task_display_id=task["display_id"],
        timeout=task_timeout,
    )
    # assert that the output is as expected
    assert task["status"] in ["completed", "success"]
    assert len(output) > 0


@pytest.mark.os("Linux")
@pytest.mark.arch("amd64")
async def test_ps(mythic_instance, task_timeout, callback_display_id):
    # issue the task and wait for it to finish
    task = await mythic.issue_task(
        mythic=mythic_instance,
        command_name="ps",
        parameters="",
        callback_display_id=callback_display_id,
        token_id=None,
        wait_for_complete=True,
        custom_return_attributes="""
            id
            status
            completed
            display_id
            callback {
                id
            }
        """,
        timeout=task_timeout
    )
    # gather all the task's output
    output = await mythic.waitfor_for_task_output(
        mythic=mythic_instance,
        task_display_id=task["display_id"],
        timeout=task_timeout,
    )
    # assert that the output is as expected
    assert task["status"] in ["completed", "success"]
    assert len(output) > 0


@pytest.mark.os("Linux")
@pytest.mark.arch("amd64")
async def test_upload(mythic_instance, task_timeout, callback_display_id, tmp_path_factory):
    upload_path =  tmp_path_factory.getbasetemp() / str(uuid4())

    # Register a test file to upload
    resp = await mythic.register_file(
        mythic=mythic_instance, filename="test.txt", contents=b"this is a test"
    )

    # issue the task and wait for it to finish
    task = await mythic.issue_task(
        mythic=mythic_instance,
        command_name="upload",
        parameters={"remote_path": str(upload_path), "file_id": resp, "overwrite":True},
        callback_display_id=callback_display_id,
        token_id=None,
        wait_for_complete=True,
        custom_return_attributes="""
            id
            status
            completed
            display_id
            callback {
                id
            }
        """,
        timeout=task_timeout
    )
    # gather all the task's output
    output = await mythic.waitfor_for_task_output(
        mythic=mythic_instance,
        task_display_id=task["display_id"],
        timeout=task_timeout,
    )
    # assert that the output is as expected
    assert os.path.isfile(upload_path)
    os.remove(upload_path)
    assert len(output) > 0


@pytest.mark.os("Linux")
@pytest.mark.arch("amd64")
async def test_download(mythic_instance, task_timeout, callback_display_id, tmp_path_factory):
    file_path =  tmp_path_factory.getbasetemp() / str(uuid4())

    # Write to the file, so there's something to download
    f = open(file_path,"w")
    f.write("Poseidon download test\n")
    f.close()

    output = await mythic.issue_task_and_waitfor_task_output(
        mythic=mythic_instance,
        command_name="download",
        parameters=str(file_path),
        callback_display_id=callback_display_id,
        timeout=60,
    )
    
    # Get the file_id from the download task
    js = json.loads(output.split(b'\n')[0])
    file_id = js['file_id']

    # Call the Mythic APIs to get the file
    file_bytes = await mythic.download_file(
        mythic=mythic_instance,
        file_uuid=file_id
    )

    os.remove(file_path)
    assert b"Poseidon" in file_bytes




@pytest.mark.os("Linux")
@pytest.mark.arch("amd64")
async def test_socks(mythic_instance, task_timeout, callback_display_id):
    # Below is the command to get the parameters required for this command
    # usage = await mythic.get_command_parameter_options(command_name="socks", payload_type_name="poseidon", mythic=mythic_instance)
    # print(usage)
    
    # issue the task and wait for it to finish
    task = await mythic.issue_task(
        mythic=mythic_instance,
        command_name="socks",
        parameters={"action":'start', "port":7001},
        callback_display_id=callback_display_id,
        token_id=None,
        wait_for_complete=True,
        custom_return_attributes="""
            id
            status
            completed
            display_id
            callback {
                id
            }
        """,
        timeout=task_timeout
    )
    # gather all the task's output
    output = await mythic.waitfor_for_task_output(
        mythic=mythic_instance,
        task_display_id=task["display_id"],
        timeout=task_timeout,
    )
    # assert that the output is as expected
    assert task["status"] in ["completed", "success"]
    
    # Try to use the new proxy - either it will work, or time out
    # How do we use the proxy when it's on the Mythic server?
    # In the Linux case, the Mythic server is where tests are being run, so this should work fine
    # When we port this for remote cases, this could get trickier
    proxies = {
    "http":"socks5://127.0.0.1:7001",
    "https":"socks5://127.0.0.1:7001",
    }
    url = "https://ipv4.icanhazip.com"

    # Making the request will raise a ConnectionError if the proxy isn't listening or forwarding properly
    r = requests.get(url,proxies=proxies)
    # In case a timeout doesn't raise an error, make sure that we got something like an IP back
    assert b"." in r.content

    # Now, stop the socks proxy
    task = await mythic.issue_task(
        mythic=mythic_instance,
        command_name="socks",
        parameters={"action":'stop', "port":7001},
        callback_display_id=callback_display_id,
        token_id=None,
        wait_for_complete=True,
        custom_return_attributes="""
            id
            status
            completed
            display_id
            callback {
                id
            }
        """,
        timeout=task_timeout
    )
    # gather all the task's output
    output = await mythic.waitfor_for_task_output(
        mythic=mythic_instance,
        task_display_id=task["display_id"],
        timeout=task_timeout,
    )
    # assert that the output is as expected
    assert task["status"] in ["completed", "success"]
  