from mythic import mythic
import pytest
import json
from . import helper_functions
import os

build_file = "poseidon_macos_arm_build.json"


async def wrapped_remote_execution_function(payload_path: str):
    return await helper_functions.remote_execution_function(payload_path=payload_path,
                                                            host=os.getenv("MACOS_ARM_REMOTE_HOST"),
                                                            password=os.getenv("MACOS_ARM_REMOTE_PASSWORD"),
                                                            user=os.getenv("MACOS_ARM_REMOTE_USER"))


@pytest.mark.os("macOS")
@pytest.mark.arch("arm")
async def test_list_entitlements(mythic_instance, task_timeout, make_callback_display_id):
    async for callback_id in make_callback_display_id(
            use_payload_build_file=build_file,
            remote_execution_function=wrapped_remote_execution_function,
            remote_cleanup_function=helper_functions.remote_cleanup_function
    ):
        output = await mythic.issue_task_and_waitfor_task_output(
            mythic=mythic_instance,
            command_name="list_entitlements",
            parameters=json.dumps({"pid": 1}),
            callback_display_id=callback_id,
            timeout=task_timeout,
        )
        output = json.loads(output)
        assert len(output) > 0
