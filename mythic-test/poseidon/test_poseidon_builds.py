from mythic import mythic
import asyncio
import pytest
import json


@pytest.mark.slow
@pytest.mark.build
@pytest.mark.parametrize("selected_os", ["macOS", "Linux"])
@pytest.mark.parametrize("architecture", ["amd64", "arm64"])
async def test_builds(mythic_instance, build_timeout, selected_os, architecture, operating_systems, architectures):
    if len(operating_systems) > 0 and selected_os not in operating_systems:
        pytest.skip(f"{selected_os} is not part of the selected operating systems: {operating_systems}")
    if len(architectures) > 0 and architecture not in architectures:
        pytest.skip(f"{architecture} is not part of the selected architectures: {architectures}")
    with open(f"./poseidon/supporting-files/poseidon_build.json", "r") as f:
        agent_config = json.load(f)
        # update the build args to replace architecture with the parameterized version
        used_arch_name = "AMD_x64" if architecture == "amd64" else "ARM_x64"
        updated_build_args = [{"name": x["name"], "value": used_arch_name} if x["name"] == "architecture" else x for x in agent_config["build_parameters"] ]
        updated_description = f"poseidon build tests: {selected_os} - {architecture}"
        payload_response = await mythic.create_payload(
            mythic=mythic_instance,
            payload_type_name=agent_config["payload_type"],
            filename=agent_config["filename"],
            operating_system=selected_os,
            commands=agent_config["commands"],
            c2_profiles=agent_config["c2_profiles"],
            build_parameters=updated_build_args,
            description=updated_description,
            return_on_complete=True,
            timeout=build_timeout,
            custom_return_attributes="""
                build_phase
                uuid
                build_stderr
                build_stdout
            """
        )
        assert payload_response is not None
        assert payload_response["build_phase"] == "success"
