from mythic import mythic
import asyncio
import pytest
import json


@pytest.mark.slow
@pytest.mark.build
@pytest.mark.parametrize("output_type", ["WinExe", "Shellcode"])
@pytest.mark.arch("amd64")
async def test_builds(mythic_instance, build_timeout, output_type):
    with open(f"./apollo/supporting-files/apollo_build.json", "r") as f:
        agent_config = json.load(f)
        # update the build args to replace architecture with the parameterized version
        updated_build_args = [{"name": x["name"], "value": output_type} if x["name"] == "output_type" else x for x in agent_config["build_parameters"] ]
        updated_description = f"apollo build tests: {output_type}"
        payload_response = await mythic.create_payload(
            mythic=mythic_instance,
            payload_type_name=agent_config["payload_type"],
            filename=agent_config["filename"],
            operating_system=agent_config["selected_os"],
            commands=agent_config["commands"],
            c2_profiles=agent_config["c2_profiles"],
            build_parameters=updated_build_args,
            description=updated_description,
            return_on_complete=True,
            timeout=build_timeout,
            custom_return_attributes="""
                build_phase
                uuid
                build_stderr
                build_stdout
            """
        )
        assert payload_response is not None
        assert payload_response["build_phase"] == "success"
