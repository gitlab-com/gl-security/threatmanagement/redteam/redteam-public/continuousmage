import winrm
import uuid
import pytest
from smb.SMBConnection import SMBConnection
import base64


async def remote_execution_function(payload_path: str,
                                    host_ip: str,
                                    host_name: str,
                                    password: str,
                                    user: str) -> (winrm.Session, str, str, str):
    if host_ip is None or host_ip == "":
        pytest.fail("need to run test against remote host but WINDOWS_REMOTE_HOST_IP not specified")
    if host_name is None or host_name == "":
        pytest.fail("need to run test against remote host but WINDOWS_REMOTE_HOST_NAME not specified")
    if user is None or user == "":
        pytest.fail("need to run test against remote host but no user specified")
    uuid_name = str(uuid.uuid4())
    remote_path = f"\\Users\\{user}\\AppData\\Local\\Temp\\{uuid_name}.exe"
    client = winrm.Session(host_ip, auth=(user, password))
    try:
        conn = SMBConnection(username=user, password=password, my_name="mythic", remote_name=host_name, use_ntlm_v2=True)
        conn.connect(host_ip)
    except Exception as e:
        pytest.fail("Failed to connect to remote host for SMB file transfer")
    with open(payload_path, "rb") as f:
        conn.storeFile("C$", remote_path, f)
    conn.close()
    print(remote_path)
    prtl = client.protocol
    script = f"Start-Process {remote_path}; Start-Sleep 3"
    encoded_ps = base64.b64encode(script.encode('utf_16_le')).decode('ascii')
    command = 'powershell -encodedcommand {0}'.format(encoded_ps)
    shell_id = prtl.open_shell()
    prtl.run_command(shell_id, command)
    return client, shell_id, uuid_name, remote_path


async def remote_cleanup_function(remote_result: (winrm.Session, str, str, str)) -> None:
    if remote_result is not None:
        remote_result[0].run_ps(f"Stop-Process -ProcessName {remote_result[2]}")
        remote_result[0].run_ps(f"Remove-Item -Path {remote_result[3]}")
        remote_result[0].protocol.close_shell(remote_result[1], close_session=True)
