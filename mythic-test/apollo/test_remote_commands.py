from mythic import mythic
import pytest
import json
from . import helper_functions
import os

build_file = "apollo_build.json"


async def wrapped_remote_execution_function(payload_path: str):
    return await helper_functions.remote_execution_function(payload_path=payload_path,
                                                            host_ip=os.getenv("WINDOWS_REMOTE_HOST_IP"),
                                                            host_name=os.getenv("WINDOWS_REMOTE_HOST_NAME"),
                                                            password=os.getenv("WINDOWS_REMOTE_PASSWORD"),
                                                            user=os.getenv("WINDOWS_REMOTE_USER"))


@pytest.mark.os("Windows")
async def test_ps(mythic_instance, task_timeout, make_callback_display_id):
    async for callback_id in make_callback_display_id(
            use_payload_build_file=build_file,
            #use_payload_uuid="38f3c276-744d-4d8b-8501-7fb3a95bb458",
            remote_execution_function=wrapped_remote_execution_function,
            remote_cleanup_function=helper_functions.remote_cleanup_function
    ):
        output = await mythic.issue_task_and_waitfor_task_output(
            mythic=mythic_instance,
            command_name="ps",
            parameters="",
            callback_display_id=callback_id,
            timeout=task_timeout,
        )
        #output = json.loads(output)
        assert len(output) > 0
